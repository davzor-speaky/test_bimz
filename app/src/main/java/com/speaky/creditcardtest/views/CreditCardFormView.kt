package com.speaky.creditcardtest.views

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.widget.EditText
import android.widget.TextView
import com.speaky.creditcardtest.R
import com.speaky.creditcardtest.domain.cards.CardsChecker
import com.speaky.creditcardtest.domain.cards.CreditCard
import com.speaky.creditcardtest.utils.afterTextChanged

class CreditCardFormView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val etCardNumber: EditText
    private val tvValid: TextView
    private val cardsChecker = CardsChecker()

    init {
        inflate(context, R.layout.main_credit_card_form, this)
        etCardNumber = findViewById(R.id.et_card_number)
        tvValid = findViewById(R.id.tv_valid)
        etCardNumber.afterTextChanged { onTextChange(it) }
    }

    private fun onTextChange(cardNumber: String) {
        val card = cardsChecker.getCreditCard(cardNumber)
        val drawableId: Int
        if (card.type == CreditCard.TYPE.UNKNOWN) {
            tvValid.text = "UNKNOWN"
            drawableId = R.drawable.main_ic_unknown
        } else {
            tvValid.text = card.type.name
            drawableId = if (card.isValid) R.drawable.main_ic_valid else R.drawable.main_ic_invalid
        }
        tvValid.setCompoundDrawablesWithIntrinsicBounds(null, null, null, ContextCompat.getDrawable(context, drawableId))
    }
}