package com.speaky.creditcardtest.utils

val Char.intValue: Int
    get() = Character.getNumericValue(this)