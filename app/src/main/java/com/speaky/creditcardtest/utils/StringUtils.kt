package com.speaky.creditcardtest.utils

fun String?.isOnlyNumbers(): Boolean = this != null && this.isNotEmpty() && this.matches("^[0-9]+$".toRegex())
