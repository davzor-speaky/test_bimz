package com.speaky.creditcardtest.domain.cards.checker

import com.speaky.creditcardtest.domain.cards.CreditCard
import com.speaky.creditcardtest.utils.intValue
import com.speaky.creditcardtest.utils.isOnlyNumbers

class MastercardChecker : CreditCardChecker {

    companion object {
        const val SIZE = 16
        const val START_NUMBER = 5
    }

    override fun isValid(cardNumber: String?): Boolean {
        return cardNumber != null && isCreditCardType(cardNumber) && cardNumber.length == SIZE && cardNumber.isOnlyNumbers()
    }

    override fun isCreditCardType(cardNumber: String?): Boolean {
        return cardNumber != null && cardNumber.isNotEmpty() && cardNumber.first().intValue == START_NUMBER;
    }

    override fun getType(): CreditCard.TYPE {
        return CreditCard.TYPE.MASTERCARD
    }
}