package com.speaky.creditcardtest.domain.cards.checker

import com.speaky.creditcardtest.domain.cards.CreditCard

interface CreditCardChecker {
    fun isCreditCardType(cardNumber: String?): Boolean

    fun isValid(cardNumber: String?): Boolean

    fun getType(): CreditCard.TYPE

}