package com.speaky.creditcardtest.domain.cards

data class CreditCard(val number: String?, val type: TYPE, val isValid: Boolean) {
    enum class TYPE {
        VISA, MASTERCARD, AMERICAN_EXPRESS, UNKNOWN
    }
}