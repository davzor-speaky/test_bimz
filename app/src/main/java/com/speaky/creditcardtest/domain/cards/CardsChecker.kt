package com.speaky.creditcardtest.domain.cards

import com.speaky.creditcardtest.domain.cards.checker.*


class CardsChecker {

    private val cardsChecker = listOf(AmericanExpressChecker(), VisaChecker(), MastercardChecker())

    fun getCreditCard(cardNumber: String?): CreditCard {
        val type = getCardType(cardNumber)
        return CreditCard(cardNumber, type.getType(), type.isValid(cardNumber))

    }

    private fun getCardType(cardNumber: String?): CreditCardChecker {
        return cardsChecker
                .firstOrNull { it.isCreditCardType(cardNumber) }
                ?: UnknownChecker()
    }
}