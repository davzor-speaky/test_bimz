package com.speaky.creditcardtest.domain.cards.checker

import com.speaky.creditcardtest.domain.cards.CreditCard
import com.speaky.creditcardtest.utils.intValue
import com.speaky.creditcardtest.utils.isOnlyNumbers

class AmericanExpressChecker : CreditCardChecker {
    companion object {
        const val SIZE = 15
        const val START_NUMBER = 3
        val SECOND_NUMBERS = listOf(4, 7)
    }

    override fun isValid(cardNumber: String?): Boolean {
        return cardNumber != null && isCreditCardType(cardNumber) && cardNumber.length == SIZE && cardNumber.isOnlyNumbers()
    }

    override fun isCreditCardType(cardNumber: String?): Boolean {
        return cardNumber != null && cardNumber.length >= 2 && cardNumber.first().intValue == START_NUMBER && SECOND_NUMBERS.contains(cardNumber[1].intValue)
    }

    override fun getType(): CreditCard.TYPE {
        return CreditCard.TYPE.AMERICAN_EXPRESS
    }

}