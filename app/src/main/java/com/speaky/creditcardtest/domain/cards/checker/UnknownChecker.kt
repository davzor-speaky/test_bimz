package com.speaky.creditcardtest.domain.cards.checker

import com.speaky.creditcardtest.domain.cards.CreditCard

class UnknownChecker : CreditCardChecker {

    override fun isValid(cardNumber: String?) = false

    override fun isCreditCardType(cardNumber: String?) = false

    override fun getType(): CreditCard.TYPE {
        return CreditCard.TYPE.UNKNOWN
    }
}