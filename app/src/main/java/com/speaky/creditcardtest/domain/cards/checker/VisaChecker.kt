package com.speaky.creditcardtest.domain.cards.checker

import com.speaky.creditcardtest.domain.cards.CreditCard
import com.speaky.creditcardtest.utils.intValue
import com.speaky.creditcardtest.utils.isOnlyNumbers

class VisaChecker : CreditCardChecker {

    companion object {
        const val MAX_SIZE = 16
        const val MIN_SIZE = 13
        const val START_NUMBER = 4
    }

    override fun isValid(cardNumber: String?): Boolean {
        return cardNumber != null && isCreditCardType(cardNumber) && cardNumber.length >= MIN_SIZE && cardNumber.length <= MAX_SIZE && cardNumber.isOnlyNumbers()
    }

    override fun isCreditCardType(cardNumber: String?): Boolean {
        return cardNumber != null && cardNumber.isNotEmpty() && cardNumber.first().intValue == START_NUMBER
    }

    override fun getType(): CreditCard.TYPE {
        return CreditCard.TYPE.VISA
    }
}