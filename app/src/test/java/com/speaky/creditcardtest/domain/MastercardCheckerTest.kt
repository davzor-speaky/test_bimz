package com.speaky.creditcardtest.domain

import com.speaky.creditcardtest.domain.cards.checker.MastercardChecker
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class MastercardCheckerTest {

    private var mastercardChecker = MastercardChecker()

    @Test
    fun testIsCreditCardIsEmpty() {
        assertThat(mastercardChecker.isCreditCardType(""), `is`(false))
    }

    @Test
    fun testIsCreditCardIsNull() {
        assertThat(mastercardChecker.isCreditCardType(null), `is`(false))
    }

    @Test
    fun testIsCreditCardIfCorrectOne() {
        assertThat(mastercardChecker.isCreditCardType("534"), `is`(true))
    }


    @Test
    fun testIsCreditCardIfCorrectTwo() {
        assertThat(mastercardChecker.isCreditCardType("537"), `is`(true))
    }

    @Test
    fun testIsCreditCardIfNotCorrect() {
        assertThat(mastercardChecker.isCreditCardType("38"), `is`(false))
    }

    @Test
    fun testIsValidIfNull() {
        assertThat(mastercardChecker.isValid(null), `is`(false))
    }

    @Test
    fun testIsValidIfEmpty() {
        assertThat(mastercardChecker.isValid(""), `is`(false))
    }

    @Test
    fun testIsValidIfCorrect() {
        assertThat(mastercardChecker.isValid("5348555556666555"), `is`(true))
    }
}