package com.speaky.creditcardtest.domain

import com.speaky.creditcardtest.domain.cards.CardsChecker
import com.speaky.creditcardtest.domain.cards.CreditCard
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class CardsCheckerTest {

    private var cardsChecker = CardsChecker()

    @Test
    fun testTypeIfEmpty() {
        assertThat(cardsChecker.getCreditCard("").type, `is`(CreditCard.TYPE.UNKNOWN))
    }

    @Test
    fun testTypeIfNull() {
        assertThat(cardsChecker.getCreditCard(null).type, `is`(CreditCard.TYPE.UNKNOWN))
    }

    @Test
    fun testVisa() {
        assertThat(cardsChecker.getCreditCard("4").type, `is`(CreditCard.TYPE.VISA))
    }

    @Test
    fun testMastercard() {
        assertThat(cardsChecker.getCreditCard("5").type, `is`(CreditCard.TYPE.MASTERCARD))
    }

    @Test
    fun testAmericanExpress() {
        assertThat(cardsChecker.getCreditCard("37").type, `is`(CreditCard.TYPE.AMERICAN_EXPRESS))
    }


}