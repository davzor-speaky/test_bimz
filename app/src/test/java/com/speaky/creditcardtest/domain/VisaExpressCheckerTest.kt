package com.speaky.creditcardtest.domain

import com.speaky.creditcardtest.domain.cards.checker.VisaChecker
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class VisaExpressCheckerTest {

    private var visaExpressChecker = VisaChecker()

    @Test
    fun testIsCreditCardIsNull() {
        assertThat(visaExpressChecker.isCreditCardType(null), `is`(false))
    }


    @Test
    fun testIsCreditCardIsEmpty() {
        assertThat(visaExpressChecker.isCreditCardType(""), `is`(false))
    }


    @Test
    fun testIsCreditCardIfCorrectOne() {
        assertThat(visaExpressChecker.isCreditCardType("434"), `is`(true))
    }


    @Test
    fun testIsCreditCardIfCorrectTwo() {
        assertThat(visaExpressChecker.isCreditCardType("437"), `is`(true))
    }

    @Test
    fun testIsCreditCardIfNotCorrect() {
        assertThat(visaExpressChecker.isCreditCardType("38"), `is`(false))
    }

    @Test
    fun testIsValidIfNull() {
        assertThat(visaExpressChecker.isValid(null), `is`(false))
    }

    @Test
    fun testIsValidIfEmpty() {
        assertThat(visaExpressChecker.isValid(""), `is`(false))
    }

    @Test
    fun testIsValidIfCorrect() {
        assertThat(visaExpressChecker.isValid("4348555556666555"), `is`(true))
    }
}