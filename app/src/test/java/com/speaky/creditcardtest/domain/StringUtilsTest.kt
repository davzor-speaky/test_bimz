package com.speaky.creditcardtest.domain

import com.speaky.creditcardtest.utils.isOnlyNumbers
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class StringUtilsTest {

    @Test
    fun test_null() {
        assertThat(null.isOnlyNumbers(), `is`(false))
    }

    @Test
    fun test_empty() {
        assertThat("".isOnlyNumbers(), `is`(false))
    }

    @Test
    fun test_not_only_number() {
        assertThat("1111".isOnlyNumbers(), `is`(true))
    }

    @Test
    fun test_only_number() {
        assertThat("11%11".isOnlyNumbers(), `is`(false))
    }


}