package com.speaky.creditcardtest.domain

import com.speaky.creditcardtest.domain.cards.checker.AmericanExpressChecker
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class AmericanExpressCheckerTest {

    private var americanExpressChecker = AmericanExpressChecker()

    @Test
    fun testIsCreditCardIsEmpty() {
        assertThat(americanExpressChecker.isCreditCardType(""), `is`(false))
    }

    @Test
    fun testIsCreditCardIsNull() {
        assertThat(americanExpressChecker.isCreditCardType(null), `is`(false))
    }

    @Test
    fun testIsCreditCardIfCorrectOne() {
        assertThat(americanExpressChecker.isCreditCardType("34"), `is`(true))
    }


    @Test
    fun testIsCreditCardIfCorrectTwo() {
        assertThat(americanExpressChecker.isCreditCardType("37"), `is`(true))
    }

    @Test
    fun testIsCreditCardIfNotCorrect() {
        assertThat(americanExpressChecker.isCreditCardType("38"), `is`(false))
    }

    @Test
    fun testIsValidIfNull() {
        assertThat(americanExpressChecker.isValid(null), `is`(false))
    }

    @Test
    fun testIsValidIfEmpty() {
        assertThat(americanExpressChecker.isValid(""), `is`(false))
    }

    @Test
    fun testIsValidIfCorrect() {
        assertThat(americanExpressChecker.isValid("348555556666555"), `is`(true))
    }
}