package com.speaky.creditcardtest

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.speaky.creditcardtest.domain.cards.CreditCard
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Before


@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {

    private var mStringToBetyped: String? = null

    @Rule
    @JvmField
    val mIntentsRule = ActivityTestRule<MainActivity>(MainActivity::class.java)


    @Test
    fun changeText_noText() {
        onView(withId(R.id.tv_valid)).check(matches(withText("")))
    }

    @Test
    fun changeText_visa() {
        onView(withId(R.id.et_card_number)).perform(typeText("4584"))
        onView(withId(R.id.tv_valid)).check(matches(withText(CreditCard.TYPE.VISA.name)))
    }

    @Test
    fun changeText_unknown() {
        onView(withId(R.id.et_card_number)).perform(typeText("1789"))
        onView(withId(R.id.tv_valid)).check(matches(withText(CreditCard.TYPE.UNKNOWN.name)))
    }

    @Test
    fun changeText_mastercard() {
        onView(withId(R.id.et_card_number)).perform(typeText("5584"))
        onView(withId(R.id.tv_valid)).check(matches(withText(CreditCard.TYPE.MASTERCARD.name)))
    }

    @Test
    fun changeText_american_express() {
        onView(withId(R.id.et_card_number)).perform(typeText("3748"))
        onView(withId(R.id.tv_valid)).check(matches(withText(CreditCard.TYPE.AMERICAN_EXPRESS.name)))
    }
}